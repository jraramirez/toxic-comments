import pandas as pd
import numpy as np
import re
import nltk

# Function for removing list of words from a list of strings
def removeWords(strings, wordList):
    cleanedStringsList = []

    for s in strings:
        # Remove words enumerated in wordList
        big_regex = re.compile('|'.join(map(re.escape, wordList)), re.IGNORECASE)
        c = big_regex.sub("", str(s))
        cleanedStringsList.append(c)

    return cleanedStringsList

#Function for removing 2 letter words
def remove2LetterWords(strings):
    cleanedStringsList = []

    for s in strings:
        s = re.sub(r'\b\w{1,2}\b', '', str(s))
        cleanedStringsList.append(str(s))

    return cleanedStringsList

# Function for removing non-alphanumeric characters
def removeSpecialCharacters(strings):
    cleanedStringsList = []

    for s in strings:
        s = re.sub(r'([^\s\w]|_)+', ' ', str(s))
        cleanedStringsList.append(str(s))

    return cleanedStringsList

# Function for removing digits from a list of strings
def removeDigits(strings):
    cleanedStringsList = []

    for s in strings:
        s = ''.join([i for i in s if not i.isdigit()])
        cleanedStringsList.append(s)
    return cleanedStringsList

def getNouns(strings):
    nounsList = []
    for s in strings:
        s = nltk.word_tokenize(s)
        nounsString = ''
        posResult = nltk.pos_tag(s)
        for p in posResult:
            if(p[1] == 'NN'):
                nounsString = nounsString + ' ' + p[0]
        nounsList.append(nounsString)
    return nounsList