# Packages
import datetime
import pandas as pd

# Constants
fileName = 'train'
inFolderName = 'data/'
outFolderName = 'data/'
fileType = '.csv'
# sheetName = 'Sheet1'
# sheetName = 'SHEET'
fraction = 0.1
nSamples = 1000
replace = False

df = pd.read_csv(inFolderName + fileName + fileType)
newDF = pd.DataFrame()
newDF = df.sample(frac=fraction, replace=replace, random_state=17)
# newDF = df.sample(n=nSamples, replace=replace, random_state=17)

# newDF.to_csv(outFolderName + fileName + ' - ' + str(nSamples) + ' samples' + fileType)
newDF.to_csv(outFolderName + fileName + ' - ' + str(fraction) + ' samples' + fileType)