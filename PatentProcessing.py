import pandas as pd
import re

# Function for creating custom confusion matrix 
def getCustomConfusionMatrix(categoriesDF):
    uniqueCategories = []
    for c in categoriesDF['Categories']:
        if(c and c == c):
            uniqueCategories.extend(c)
    uniqueCategories = list(sorted(set(uniqueCategories)))
    categoryFrequencies = pd.DataFrame(index=uniqueCategories)
    categoryFrequencies['Correct'] = 0
    categoryFrequencies['Incorrect'] = 0
    for index, row in categoriesDF.iterrows():
        if(row['Categories'] and row['Categories'] == row['Categories']):
            for pc in row['Predicted Categories']:
                if(pc in uniqueCategories):
                    if pc in row['Categories']:
                        categoryFrequencies['Correct'][pc] = categoryFrequencies['Correct'][pc] + 1
                    else:
                        categoryFrequencies['Incorrect'][pc] = categoryFrequencies['Incorrect'][pc] + 1
    categoryFrequencies['% Correct'] = categoryFrequencies['Correct']/(categoryFrequencies['Correct'] + categoryFrequencies['Incorrect'])
    return categoryFrequencies

# Function for labeling relevant data
def labelRelevantData(df, titlesColumnName, abstractsColumnName, claimsColumnName):
    phraseList = []
    with open('phrases.txt', 'r') as f:
        for line in f:
            phraseList.append(line)
    f.close()

    count = 0
    trueCount = 0
    cc = 0
    e1 = 0
    e2 = 0
    r = re.compile('|'.join(phraseList), re.IGNORECASE)

    for index, row in df.iterrows():
        titleHasTerms = abstractHasTerms = claimHasTerms = False
        if(r.search(str(row[titlesColumnName]))):
            titleHasTerms = True
        if(r.search(str(row[abstractsColumnName]))):
            abstractHasTerms = True
        if(r.search(str(row[claimsColumnName]))):
            claimHasTerms = True

    newDF = df.copy(deep=True)
    newDF['relevant'] = 0
    for index, row in newDF.iterrows():
        titleHasTerms = abstractHasTerms = claimHasTerms = False
        if(r.search(str(row[titlesColumnName]))):
            titleHasTerms = True
        if(r.search(str(row[abstractsColumnName]))):
            abstractHasTerms = True
        if(r.search(str(row[claimsColumnName]))):
            claimHasTerms = True

        # for p in phraseList:
        #     if p.lower() in str(row[titlesColumnName]).lower():
        #         titleHasTerms = True
        #     if p.lower() in str(row[abstractsColumnName]).lower():
        #         abstractHasTerms = True
        #     if p.lower() in str(row[claimsColumnName]).lower():
        #         claimHasTerms = True
        
        # titleHasTerms =  re.search(r'(mixed reality|augmented reality)', str(row[titlesColumnName]), re.IGNORECASE)
        # abstractHasTerms = re.search(r'(mixed reality|augmented reality)', str(row[abstractsColumnName]), re.IGNORECASE)
        # claimHasTerms = re.search(r'(mixed reality|augmented reality)', str(row[claimsColumnName]), re.IGNORECASE)
        
        if(titleHasTerms or abstractHasTerms or claimHasTerms):
            newDF.loc[index, 'relevant'] = 1
            count = count + 1
            if(str(row['Relevancy Check']).lower() == 'y'):
                cc = cc + 1
        if(str(row['Relevancy Check']).lower() == 'y'):
            trueCount = trueCount + 1
        if((titleHasTerms or abstractHasTerms or claimHasTerms) and str(row['Relevancy Check']).lower() != 'y'):
            e1 = e1 + 1
        if(not (titleHasTerms or abstractHasTerms or claimHasTerms) and str(row['Relevancy Check']).lower() == 'y'):
            e2 = e2 + 1

    print('Number of relevant PPA: ' + str(count))
    print('% Correct: ' + str(cc/trueCount*100))
    print('type 1: ' + str(e1/5980*100))
    print('type 2: ' + str(e2/5980*100))

    return newDF

# Function for labeling data with categories
def labelDataWithCategories(df, categoryColumnName):
    newDF = df.copy(deep=True)
    newDF['hasCategory'] = 0
    for index, row in newDF.iterrows():
        if(row[categoryColumnName] and row[categoryColumnName] == row[categoryColumnName]):
            newDF.loc[index, 'hasCategory'] = 1
    return newDF
