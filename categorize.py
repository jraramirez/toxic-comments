# Packages
import numpy as np
import pandas as pd
import datetime
import ast

from sklearn.metrics import accuracy_score
from sklearn.pipeline import Pipeline
from sklearn.pipeline import FeatureUnion
from sklearn.feature_extraction.text import TfidfVectorizer
from sklearn.preprocessing import MultiLabelBinarizer
from sklearn.preprocessing import OneHotEncoder
from sklearn.feature_extraction import DictVectorizer
from sklearn.model_selection import train_test_split

# One vs Rest Classifiers
from sklearn.multiclass import OneVsRestClassifier
from sklearn.ensemble import GradientBoostingClassifier
# from sklearn.ensemble import RandomForestClassifier

import ClaimsCleaner as cc
import PatentProcessing as pp
import TFIDFAnalyzer as ta
import ItemSelector as ise
import PlainVectorizer as pv

# Constants
trainFileName = 'train'
testFileName = 'test'
inFolderName = 'data/'
outFolderName = 'out/'
fileType = '.csv'
idColumnName = 'id'
commentTextColumnName = 'comment_text'
classes = ['toxic', 'severe_toxic', 'obscene', 'threat', 'insult', 'identity_hate']

df = pd.read_csv(inFolderName + trainFileName + fileType, encoding = 'latin1')
dfTrueTest = pd.read_csv(inFolderName + testFileName + fileType, encoding = 'latin1')

# Split to training and test data
dfTrain, dfTest = train_test_split(df, test_size=0.5, random_state=42)

# Prepare Y
trainY = dfTrain.iloc[:,2:9]
# Combining selected features
trainFeaturesDF = pd.DataFrame()
temp = cc.removeSpecialCharacters(dfTrain[commentTextColumnName])
temp = cc.removeDigits(temp)
trainFeaturesDF['Comment Text'] = temp
trainFeaturesDF['Nouns'] = cc.getNouns(trainFeaturesDF['Comment Text'])

# Model fitting
pipeline = Pipeline([
    ('union', FeatureUnion(
        transformer_list=[
            # Pipeline for pulling comment text
            ('comment text', Pipeline([
                ('selector', ise.ItemSelector(key='Comment Text')),
                ('vectorizer', TfidfVectorizer(stop_words='english', lowercase=False, ngram_range=(1,2), max_df=0.8)),
                # ('vectorizer', TfidfVectorizer(stop_words='english', lowercase=True, ngram_range=(1, 2), max_df=0.9)),
            ])),

            # Pipeline for pulling nouns
            ('nouns', Pipeline([
                ('selector', ise.ItemSelector(key='Nouns')),
                ('vectorizer', TfidfVectorizer(stop_words='english', lowercase=True, ngram_range=(1,21), max_df=0.8)),
                # ('vectorizer', TfidfVectorizer(stop_words='english', lowercase=True, ngram_range=(1, 2), max_df=0.9)),
            ])),
        ],
    )),

    ('clf', OneVsRestClassifier(GradientBoostingClassifier(n_estimators=500, learning_rate=0.05))),
    # ('clf', OneVsRestClassifier(GradientBoostingClassifier(n_estimators=10))),
])

pipeline.fit(trainFeaturesDF, trainY)
predicted = pipeline.predict(trainFeaturesDF)
predictedProbabilities = pipeline.predict_proba(trainFeaturesDF)
print("\nTrain Accuracy: ", accuracy_score(trainY, predicted))

# WORD/PHRASE IMPORTANCES
commentTextWords = pipeline.steps[0][1].transformer_list[0][1].named_steps['vectorizer'].get_feature_names()
nounsTextWords = pipeline.steps[0][1].transformer_list[1][1].named_steps['vectorizer'].get_feature_names()
words = commentTextWords + nounsTextWords

print(pipeline.named_steps['clf'].estimators_)
print(len(pipeline.named_steps['clf'].estimators_))

f = open(outFolderName + 'TrainWordRanking.txt','w')
for estimator in range(len(pipeline.named_steps['clf'].estimators_)):
    importances = pipeline.named_steps['clf'].estimators_[estimator].feature_importances_
    indices = np.argsort(importances)[::-1]
    f.write("\n\nTag Words for Category: " + classes[estimator] + "\n")
    for i in range(15):
        if(indices[i] < len(commentTextWords)):
            feature = 'Comment Text'
        else:
            feature = 'Noun'
        word = words[indices[i]]
        importance = importances[indices[i]]
        if(importance):
            # f.write("\n%d. %s (%f) %d" % (i + 1, word, importance, indices[i]))
            f.write("\n%d. %s %f (%s)" % (i + 1, word, importance, feature))
        else:
            break
f.close()

# Inverse transform categories
trainCategories = []
for index, row in trainY.iterrows():
    classList = []
    c = 0
    for i in row:
        if(i):
            classList.append(classes[c])
        c = c + 1
    trainCategories.append(classList)

# Inverse transform predicted categories
predictedCategories = []
for row in predicted:
    classList = []
    c = 0
    for i in row:
        if(i):
            classList.append(classes[c])
        c = c + 1
    predictedCategories.append(classList)

# ACCURACY MEASURE (TRAINING)
allCategoriesDF = pd.DataFrame()
allCategoriesDF['Categories'] = trainCategories
allCategoriesDF['Predicted Categories'] = predictedCategories
customConfusionMatrix  = pp.getCustomConfusionMatrix(allCategoriesDF)
correctness = customConfusionMatrix['Correct'].sum()/(customConfusionMatrix['Correct'].sum() + customConfusionMatrix['Incorrect'].sum())
print('\n% Correct Overall (Training): ', str(correctness))

f = open(outFolderName + 'trainConfusionMatrix.txt','w')
f.write('% Correct per Category (Training):\n')
f.write(str(customConfusionMatrix))
f.close()

# TESTING THE MODEL
# Combining selected features
testFeaturesDF = pd.DataFrame()
temp = cc.removeSpecialCharacters(dfTest[commentTextColumnName])
temp = cc.removeDigits(temp)
testFeaturesDF['Comment Text'] = temp
testFeaturesDF['Nouns'] = cc.getNouns(testFeaturesDF['Comment Text'])

# Prepare Y
testY = dfTest.iloc[:,2:9]

predicted = pipeline.predict(testFeaturesDF)
predictedProbabilities = pipeline.predict_proba(testFeaturesDF)
print("Test Accuracy: ", accuracy_score(testY, predicted))

# Inverse transform categories
testCategories = []
for index, row in testY.iterrows():
    classList = []
    c = 0
    for i in row:
        if(i):
            classList.append(classes[c])
        c = c + 1
    testCategories.append(classList)

# Inverse transform predicted categories
predictedCategories = []
for row in predicted:
    classList = []
    c = 0
    for i in row:
        if(i):
            classList.append(classes[c])
        c = c + 1
    predictedCategories.append(classList)

# ACCURACY MEASURE (TEST)
allCategoriesDF = pd.DataFrame()
allCategoriesDF['Categories'] = testCategories
allCategoriesDF['Predicted Categories'] = predictedCategories
customConfusionMatrix  = pp.getCustomConfusionMatrix(allCategoriesDF)
correctness = customConfusionMatrix['Correct'].sum()/(customConfusionMatrix['Correct'].sum() + customConfusionMatrix['Incorrect'].sum())
print('\n% Correct Overall (Test): ', str(correctness))

f = open(outFolderName + 'testConfusionMatrix.txt','w')
f.write('% Correct per Category (Test):\n')
f.write(str(customConfusionMatrix))
f.close()

# TRUE TEST
# Combining selected features
trueTestFeaturesDF = pd.DataFrame()
temp = cc.removeSpecialCharacters(dfTrueTest[commentTextColumnName])
temp = cc.removeDigits(temp)
trueTestFeaturesDF['Comment Text'] = temp
trueTestFeaturesDF['Nouns'] = cc.getNouns(trueTestFeaturesDF['Comment Text'])

predicted = pipeline.predict(trueTestFeaturesDF)
predictedProbabilities = pipeline.predict_proba(trueTestFeaturesDF)
trueTestIDList = list(dfTrueTest['id'])
count = 0

newDF = pd.DataFrame(columns=['id','toxic', 'severe_toxic', 'obscene', 'threat', 'insult', 'identity_hate'])
newDF['id'] = dfTrueTest['id']
count = 0
for index, row in newDF.iterrows():
    newDF.loc[index, 'id'] = str(newDF.loc[index, 'id'])
    newDF.loc[index, 'toxic'] = predictedProbabilities[count][0]
    newDF.loc[index, 'severe_toxic'] = predictedProbabilities[count][1]
    newDF.loc[index, 'obscene'] = predictedProbabilities[count][2]
    newDF.loc[index, 'threat'] = predictedProbabilities[count][3]
    newDF.loc[index, 'insult'] = predictedProbabilities[count][4]
    newDF.loc[index, 'identity_hate'] = predictedProbabilities[count][5]
    count = count + 1
newDF.to_csv(outFolderName + testFileName + ' - Probabilities ' + datetime.datetime.now().strftime("%Y%m%d-%H%M") + fileType)
