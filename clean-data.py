# Packages
import numpy as np
import pandas as pd
import datetime

from sklearn.metrics import accuracy_score
from sklearn.metrics import confusion_matrix
from sklearn.metrics import adjusted_rand_score
from sklearn.metrics import classification_report
from sklearn.pipeline import Pipeline
from sklearn.pipeline import FeatureUnion
from sklearn.feature_extraction.text import CountVectorizer
from sklearn.feature_extraction.text import TfidfTransformer
from sklearn.feature_extraction.text import TfidfVectorizer
from sklearn.base import BaseEstimator, TransformerMixin
from sklearn.preprocessing import MultiLabelBinarizer
from sklearn.model_selection import train_test_split

# One vs Rest Classifiers
from sklearn.multiclass import OneVsRestClassifier
from sklearn.svm import LinearSVC
from sklearn.ensemble import GradientBoostingClassifier
from sklearn.ensemble import RandomForestClassifier

import ClaimsCleaner as cc
import TFIDFAnalyzer as ta
import ItemSelector as ise
import PatentProcessing as pp

# Constants
fileName = 'train - 0.1 samples'
fileType = '.csv'
sheetName = 'LR2050 FOR SALE MR'
sheetName = 'SHEET'
inFolderName = 'data/'
outFolderName = 'out/'
# TODO: read common words from a text file
commonWords = [
    'comprising', 
]

commentTextColumnName = 'comment_text'
df = pd.read_csv(inFolderName + fileName + fileType, encoding = 'latin1')

# Data cleaning and transformation
df = df.fillna('')

# Cleaning of titles
temp = cc.removeWords(df[commentTextColumnName], commonWords)
temp = cc.removeSpecialCharacters(temp)
df['cleanCommentText'] = cc.remove2LetterWords(temp)

df.to_csv(inFolderName + fileName + ' - Clean ' + datetime.datetime.now().strftime("%Y%m%d-%H%M") + fileType)
